export const radius = 14.9
export const height = 4
export const perimeter = radius * 2 * Math.PI
export const ratio = perimeter / height
export const rotation = 193
