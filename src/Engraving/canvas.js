export let canvas = document.createElement('canvas')
export let ctx = canvas.getContext('2d')
export let props = {}


export let init = ({ width, height, fontSize }) => {

    Object.assign(canvas.style, {

        width: `${800}px`,
        height: `${800 * height / width}px`,
        border: `1px solid black`,
        backgroundColor: '#333',

    })

    Object.assign(canvas, {

        width,
        height,

    })

    Object.assign(props, {

        width,
        height,
        fontSize,

    })

}

export let render = text => {

    text = text.toUpperCase()

    let { width, height, fontSize } = props

    ctx.clearRect(0, 0, width, height)

    ctx.fillStyle = 'white'
	ctx.font = `${fontSize}px HennessySans, Arial`

    let { width:textWidth } = ctx.measureText(text)
    let delta = (width - textWidth) / 2

	ctx.fillText(text, delta, height / 2 + fontSize / 2 * .7)

    Object.assign(props, {

        text,

    })

}

// document.body.append(canvas)
