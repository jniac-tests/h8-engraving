import React, { Component } from 'react'
import * as THREE from 'three'

import carafeBase64 from './carafe.js'

import * as cylinder from './cylinder.js'
import * as EngravingUtils from './engraving-utils.js'
import * as EngravingCanvas from './canvas.js'

EngravingUtils.init(THREE)



let width = 950, height = 768

EngravingCanvas.init({ width: 200 * cylinder.ratio, height: 200, fontSize: 96 })

export default class Engraving extends Component {

    static defaultProps = { text: 'Hennessy Engraving!', onCanvas: null }

    componentDidMount() {

        this.init()

    }

    init = async () => {

        let { canvas } = this.refs
        let { onCanvas, onFrame } = this.props

        let scene = new THREE.Scene()

        scene.onFrame(onFrame)

        let camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000)
        let renderer = new THREE.WebGLRenderer({ canvas, alpha: true })
        renderer.setSize(width, height)
        renderer.setPixelRatio(2)

        new THREE.Mesh(new THREE.BoxGeometry(1, 1, 1), new THREE.MeshBasicMaterial({ color: '#999' }))
            .setPositionY(.08)
            .setScale(.075)
            // .addTo(scene)
            .onFrame(cube => {

                cube.rotation.x += 0.1
                cube.rotation.y += 0.1

            })

        let tube = EngravingUtils.createTube({ radius: '14.9 cm', height: '4 cm' }).fromC4D('', '8 cm', '-3.191 cm').setRotationY(cylinder.rotation).addTo(scene)
        tube.textureMat.map = new THREE.Texture(EngravingCanvas.canvas)
        tube.textureMat.map.minFilter = THREE.LinearFilter
        tube.textureMat.map.needsUpdate = true

        camera.fromC4D('13.802 cm', '7.414 cm', '-60 cm', '13.189 °', '-1.274 °', '0', 'HPB')
        camera.fov = 32.453
        camera.updateProjectionMatrix()

        Object.assign(this, {

            scene,
            camera,
            renderer,
            tube,

        })

        Object.assign(window, this)

        let animate = () => {

            requestAnimationFrame(animate)

            if (Date.now() - this.timestamp > 1000)
                return

            scene.executeOnFrame()
            renderer.render(scene, camera)

        }

        this.setText(this.props.text)

        animate()

        if (onCanvas)
            onCanvas(canvas)

    }

    timestamp = Date.now()

    requestUpdate = () => {

        this.timestamp = Date.now()

    }

    setText = (text = this._text) => {

        EngravingCanvas.render(text)
        this._text = text

        let { tube } = this

        tube.material.map.needsUpdate = true

        this.requestUpdate()

    }

    get text() { return this._text }
    set text(value) { this.setText(value) }

    getRotation = () => this.tube.rotation.degree.y - cylinder.rotation
    setRotation = value => {

        this.tube.rotation.degree.y = value + cylinder.rotation
        this.requestUpdate()


    }

    get rotation() { return this.getRotation() }
    set rotation(value) { this.setRotation(value) }

    render() {

        let { onCanvas, onFrame, style, ...props } = this.props

        return (
            <div {...props} style={{ position: 'relative', width, height, ...style }} onMouseMove={() => this.requestUpdate()}>
                <img src={carafeBase64} style={{ ...styles.solid,  width, height }} alt='background'/>
                <canvas ref='canvas' style={styles.solid}></canvas>
            </div>
        )

    }

}

let styles = {

    solid: {

        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,

    },

}
