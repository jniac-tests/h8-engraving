import React from 'react'

import { Shaders, Node, GLSL } from 'gl-react'
import { Surface } from 'gl-react-dom'

import Sprite from './utils/Sprite.js'
import FrameComponent from './utils/FrameComponent.js'
import Scene3D from './Scene3D'
import carafeBase64 from './carafe.js'

import { Blur } from './gl/Blurred.js'

const shaders = Shaders.create({

    White: {

        frag: GLSL`

            precision highp float;
            void main() {
                gl_FragColor = vec4(1.,1.,1.,1.);
            }

        `

    },

    Texture: {

        frag: GLSL`

            precision highp float;
            varying vec2 uv;
            uniform sampler2D texture;
            void main() {
                gl_FragColor = texture2D(texture, uv);
            }

        `

    },

    UV: {

        frag: GLSL`

            precision highp float;
            varying vec2 uv;
            void main() {
              gl_FragColor = vec4(uv.x, uv.y, 0.5, 1.0);
            }

        `

    },

    Stencil: {

        frag: GLSL`

            precision highp float;
            varying vec2 uv;
            uniform sampler2D texture, mask;
            void main() {
                gl_FragColor = texture2D(texture, uv) * texture2D(mask, uv).a;
            }

        `

    },

    Engrave: {

        frag: GLSL`

            precision highp float;
            varying vec2 uv;
            uniform sampler2D image, imageBlur, text, textBlur;
            float f1(float x) {
                return 1. - pow((x < .5 ? .5-x : x-.5) *  2., 3.);
            }
            void main() {

                vec4 base = texture2D(imageBlur, uv);
                base *= 2.8;
                base += .1;
                base += (1. - texture2D(textBlur, uv)) * .2;

                float a = texture2D(text, uv).a;
                a *= mix(.25, 1., f1(uv.x));
                gl_FragColor = mix(texture2D(image, uv), base, a);

            }

        `

    },

})

export const White = () => <Node shader={shaders.White}/>

export const Texture = ({ children:texture }) => <Node shader={shaders.Texture} uniforms={{ texture }}/>

export const Stencil = ({ children }) => {

    if (!children && children.length !== 2) {

        console.warn(`Stencil must have two children`)

        return <Node/>

    }

    let [texture, mask] = children

    return <Node shader={shaders.Stencil} uniforms={{ texture, mask }}/>

}

export const Engrave = props => <Node shader={shaders.Engrave} uniforms={props}/>




export default class Engraving extends FrameComponent {

    static defaultProps = { width: 950, height: 768, text: 'Engraving' }

    state = { canvas: null }

    redrawCanvas = redraw => {

        this.onFrame(redraw)

        let { canvas } = this.state

        return canvas

    }

    setText = (text = this.text) => {

        let { Scene3D, input, invisibleInput } = this.refs

        this.text = text
        Scene3D.setText(text)
        invisibleInput.value = text

        if (input)
            input.value = text

        // postMessage to allow webview
        window.postMessage(JSON.stringify({ type: 'text', text }), '*')

    }

    onSubmit = event => {

        event.preventDefault()

        this.refs.invisibleInput.blur()

    }

    onDrag = ({ delta: { x } }) => this.refs.Scene3D.rotation += x / 15

    onTrapTap = event => {

        event.preventDefault()
        event.stopPropagation()
        this.refs.invisibleInput.focus()

    }

    invisibleOnFocus = () => {

        this.textOnFocus = this.text
        this.setText('')
        this.refs.Scene3D.rotation = 0

    }

    invisibleOnBlur = () => {

        if (!this.text)
            this.setText(this.textOnFocus)

    }

    componentDidMount() {

        this.setText(this.props.text)

    }

    render() {

        let { width, height, text, style, ...props } = this.props
        let { canvas } = this.state
        let { showInputs, trapColor } = window.EngravingConfig

        return (
            <Sprite {...props} style={{ width, height, ...style }}>

                <Scene3D ref='Scene3D' onCanvas={canvas => this.setState({ canvas })} onFrame={() => this.executeOnFrame()} style={{ display: 'none' }}/>

                {canvas &&
                    <Surface width={width} height={height}>
                        <Engrave
                            image={carafeBase64}
                            imageBlur={<Blur passes={8} factor={15}>{carafeBase64}</Blur>}
                            text={this.redrawCanvas}
                            textBlur={<Blur passes={2} factor={3}>{this.redrawCanvas}</Blur>}
                        />
                    </Surface>
                }

                <Sprite solid column>

                    <Sprite solid onDrag={this.onDrag}/>

                    <form
                        action=''
                        style={{ opacity: 0, width: '80%', height: '15%', marginTop: '29%', alignSelf: 'center' }}
                        onSubmit={this.onSubmit}
                    >

                        <input
                            ref='invisibleInput'
                            type='text'
                            style={{ width: '100%', height: '100%', alignSelf: 'stretch', fontSize: '62px' }}
                            onFocus={this.invisibleOnFocus}
                            onBlur={this.invisibleOnBlur}
                            onChange={event => this.setText(event.target.value)}
                        />

                    </form>

                    <Sprite solid={{ width: 300, height: 200, backgroundColor: trapColor, bottom: 0, left: '50%' }} onTouchStart={this.onTrapTap}/>

                </Sprite>

                {showInputs &&
                    <Sprite solid column transparent style={{ padding: 8, justifyContent: 'flex-end', color: '#fffc' }}>

                        <input ref='input' type='text' onChange={event => this.setText(event.target.value)} style={{ marginTop: 4, width: '100%' }}/>

                        <Sprite center onDrag={this.onDrag} style={{ height: 57, backgroundColor: '#fff2', userSelect: 'none', marginTop: 8 }}>
                            [Drag here]
                        </Sprite>

                    </Sprite>
                }

            </Sprite>
        )
    }

}
