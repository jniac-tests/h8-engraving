import { Component } from 'react'

export default class FrameComponent extends Component {

    onFrame(callback) {

        if (!callback)
            return

        if (Array.isArray(callback)) {

            for (let child of callback)
                this.onFrame(child)

            return

        }

        this.onFrameArray = this.onFrameArray || []
        this.onFrameArray.push(callback)

    }

    executeOnFrame() {

        let { onFrameArray } = this

        if (onFrameArray) {

            let tmp = [...onFrameArray]
            onFrameArray.length = 0

            this.onFrameArray = tmp.filter(callback => callback(this) !== false).concat(onFrameArray)

        }

    }

}
