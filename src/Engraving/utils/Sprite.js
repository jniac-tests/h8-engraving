import React, { Component } from 'react'
import './Sprite.css'

class Mouse {

    constructor(x, y) {

        Object.assign(this, {

            p: { x, y },
            old: { x, y },
            delta: { x: 0, y: 0 },
            timestamp: Date.now(),

        })

    }

    update(x, y) {

        let { p, old, delta } = this

        Object.assign(delta, {

            x: x - old.x,
            y: y - old.y,

        })

        Object.assign(old, p)
        Object.assign(p, { x, y })

    }

}

export default class Sprite extends Component {

    startMouse(x, y) {

        this.pressed = true
        this.mouse = new Mouse(x, y)
        this.mouseMove = new Mouse(x, y)
        this.onFrameDown()

    }

    stopMouse() {

        this.pressed = false

    }

    onFrameDown = () => {

        if (this.pressed)
            requestAnimationFrame(this.onFrameDown)

        let { x, y } = this.mouseMove.p

        this.mouse.update(x, y)

        let { onDrag } = this.props

        if (onDrag)
            onDrag(this.mouse)

    }

    // MouseEvent

    onMouseDown = ({ pageX:x, pageY:y }) => {

        this.startMouse(x, y)

        document.body.addEventListener('mouseup', this.onMouseUp)
        document.body.addEventListener('mousemove', this.onMouseMove)

    }

    onMouseMove = ({ pageX:x, pageY:y }) => this.mouseMove.update(x, y)

    onMouseUp = mouseEvent => {

        this.stopMouse()

        document.body.removeEventListener('mouseup', this.onMouseUp)
        document.body.removeEventListener('mousemove', this.onMouseMove)

    }

    // TouchEvent

    onTouchStart = event => {

        let { touches: [{ pageX:x, pageY:y }] } = event

        this.startMouse(x, y)

        document.body.addEventListener('touchmove', this.onTouchMove)
        document.body.addEventListener('touchend', this.onTouchEnd)

        if (this.props.onTouchStart)
            this.props.onTouchStart(event, this.mouse)

    }

    onTouchMove = ({ touches: [{ pageX:x, pageY:y }] }) => this.mouseMove.update(x, y)

    onTouchEnd = () => {

        this.stopMouse()

        document.body.removeEventListener('touchmove', this.onTouchMove)
        document.body.removeEventListener('touchend', this.onTouchEnd)

    }

    componentDidMount() {

        this.refs.div.addEventListener('mousedown', this.onMouseDown)
        this.refs.div.addEventListener('touchstart', this.onTouchStart)

    }

    componentWillUnmount() {

        this.refs.div.removeEventListener('mousedown', this.onMouseDown)
        this.refs.div.removeEventListener('touchstart', this.onTouchStart)

    }

    render() {

        let {

            className,
            onDrag,
            solid, column, center, flex, flexBasis,
            transparent, style,
            onTouchStart,
            ...props

        } = this.props

        style = {

            display: 'flex',
            ...style,

        }

        if (flex || flex === 0) {

            if (flex === true)
                flex = 1

            style = {

                ...style,
                flex,

            }

        }

        if (flexBasis || flexBasis === 0) {

            style = {

                ...style,
                flexBasis,

            }

        }

        if (solid) {

            if (typeof solid === 'object') {

                style = {

                    position: 'absolute',
                    ...solid

                }

            } else {

                style = {

                    position: 'absolute',
                    display: 'flex',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    ...style,

                }

            }

        }

        if (column) {

            style = {

                ...style,
                display: 'flex',
                flexDirection: 'column',

            }

        }

        if (center) {

            style = {

                ...style,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',

            }

        }

        className = className ? `sprite ${className}` : 'sprite'

        if (transparent)
            className += ' transparent'

        return <div className={className} ref='div' {...props} style={style}/>

    }

}
