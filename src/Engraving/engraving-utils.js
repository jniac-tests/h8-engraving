const unitFactor = {

	'': 	1,
	'cm': 	1e-2,
	'mm': 	1e-3,
	'°': 	Math.PI / 180,

}

const toReal = (value = '') => {

	const unit = value.match(/[a-z°]*$/)
	const r = parseFloat(value)
	return r * unitFactor[unit] || 0

}

const DEGREES = 180 / Math.PI
const RADIANS = Math.PI / 180

let THREE = null

export function init(_THREE) {

    THREE = _THREE

    Object.assign(THREE.Euler.prototype, {

    	xyzDeg(x, y, z, order) {

    		if (x === undefined && y === undefined && z === undefined)
    			return [this.x, this.y, this.z].map(v => v * DEGREES)

    		this.x = (x || 0) * RADIANS
    		this.y = (y || 0) * RADIANS
    		this.z = (z || 0) * RADIANS

    		return this

    	},

    	xDeg(value) {
    		if (value === undefined)
    			return this.x * DEGREES
    		this.x = value * RADIANS
    		return this
    	},

    	yDeg(value) {
    		if (value === undefined)
    			return this.y * DEGREES
    		this.y = value * RADIANS
    		return this
    	},

    	zDeg(value) {
    		if (value === undefined)
    			return this.z * DEGREES
    		this.z = value * RADIANS
    		return this
    	},

    })

	Object.defineProperties(THREE.Euler.prototype, {

		// trap to allow: Object3D.rotation.degree++
		degree: {

			get() {

				return this._degree || (this._degree = Object.defineProperties({}, {

					x: {
						get: () => this.x * DEGREES,
						set: value => this.x = value * RADIANS,
					},

					y: {
						get: () => this.y * DEGREES,
						set: value => this.y = value * RADIANS,
					},

					z: {
						get: () => this.z * DEGREES,
						set: value => this.z = value * RADIANS,
					},

				}))

			}

		},

	})

    Object.assign(THREE.Object3D.prototype, {

		walk(callback) {

			callback(this)

			for (let child of this.children)
				child.walk(callback)

		},

		onFrame(callback) {

			if (!callback)
				return

			if (Array.isArray(callback)) {

				for (let child of callback)
					this.onFrame(child)

				return

			}

			this.onFrameArray = this.onFrameArray || []
			this.onFrameArray.push(callback)

		},

		executeOnFrame() {

			let { onFrameArray } = this

			if (onFrameArray) {

				let tmp = [...onFrameArray]
				onFrameArray.length = 0

				this.onFrameArray = tmp.filter(callback => callback(this) !== false).concat(onFrameArray)

			}

			for (let child of this.children)
				child.executeOnFrame()

		},

        addTo(parent) {

            parent.add(this)

            return this

        },

        setScale(x, y, z) {

            if (y === undefined)
                y = x

            if (z === undefined)
                z = x

            this.scale.set(x, y, z)

            return this

        },

		setPosition(x, y, z) {

			this.position.set(x, y, z)

			return this

		},

		setPositionX(value) {

			this.position.x = value

			return this

		},

		setPositionY(value) {

			this.position.y = value

			return this

		},

		setPositionZ(value) {

			this.position.z = value

			return this

		},

		setRotation(x, y, z) {

			this.rotation.set(x * RADIANS, y * RADIANS, z * RADIANS)

			return this

		},

		setRotationX(value) {

			this.rotation.x = value * RADIANS

			return this

		},

		setRotationY(value) {

			this.rotation.y = value * RADIANS

			return this

		},

		setRotationZ(value) {

			this.rotation.z = value * RADIANS

			return this

		},

        fromC4D(px, py, pz, rx, ry, rz, order) {

        	this.position.x = toReal(px)
        	this.position.y = toReal(py)
        	this.position.z = -toReal(pz)

            if (order === 'HPB')
        		{[rx, ry] = [ry, rx]}

        	this.rotation.order = 'YXZ'
        	this.rotation.x = toReal(rx)
        	this.rotation.y = toReal(ry)
        	this.rotation.z = -toReal(rz)

        	return this

        },

    })

}

export function createTube ({ radius, height }) {

	if (typeof radius === 'string')
		radius = toReal(radius)

	if (typeof height === 'string')
		height = toReal(height)

	let geometry = new THREE.CylinderGeometry(radius, radius, height, 120, 4, true)
	let wireframeMat = new THREE.MeshBasicMaterial({ wireframe: true })
	let textureMat = new THREE.MeshBasicMaterial({ transparent: true })

	let tube = new THREE.Mesh(geometry, textureMat)

	let debug = false

	Object.assign(tube, {

		wireframeMat,
		textureMat,

		debugToggle() {

			debug = !debug
			tube.material = debug ? wireframeMat : textureMat

		},

	})

	Object.defineProperties(tube, {

		debug: {

			get: () => debug,
			set: value => {

				debug = value
				tube.material = debug ? wireframeMat : textureMat

			}

		}

	})

	return tube

}
