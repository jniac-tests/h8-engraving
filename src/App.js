import React, { Component } from 'react'
import './App.css'
import './fontfaces.css'

import Engraving from './Engraving'

// configuration
window.EngravingConfig = {

    text: 'hello gl-react & GLSL',   // default text
    showInputs: false,                      // down big buttons and inputs (desktop experience, debug...)
    trapColor: '#f000',                     // color of the trap (tapable area to fake react-native/WebView interaction)

}

window.appInit = props => {

    Object.assign(window.EngravingConfig, {

        ...props,
        appIsInitialized: true,

    })

    if (App && App.instance)
        App.instance.ensureReady()

}

// appInit test
// window.appInit({ text: `o' com'on, let some custom text` })


class App extends Component {

    static instance = this

    state = { ready: false }

    componentDidMount() {

        window.appIsInitialized
            ? this.ensureReady()
            : setTimeout(this.ensureReady, 1000)    // timeout to let injected javascript time to config global vars

        // force document not to scroll
        this.refs.div.addEventListener('touchmove', event => event.preventDefault())

    }

    ensureReady = () => {

        let { ready } = this.state

        if (!ready)
            this.setState({ ready: true })

    }

	render() {

        let { ready } = this.state

        let style = {

            // scale up because the picture is 950x768 and not 1024x768
            transform: `scale(${1024/950})`,

        }

		return (

			<div ref='div' className="App">

				{ready
                    ? <Engraving text={window.EngravingConfig.text} style={style}/>
                    : <div style={{ transform: 'scale(0)' }}>hello HennessySans</div>
                }

			</div>

		)

	}
}

export default App
