#!/usr/bin/env node

require('colors')
const fs = require('fs-extra')

const outputPath = 'build/one-file/index.html'

const meta =
    `<meta name="apple-mobile-web-app-capable" content="yes">` +
    `<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">`

const pathFromIndex = pattern => {

    let path = 'build' + index.match(new RegExp(pattern, 'm'))[1]

    return fs.readFileSync(path, 'utf8')

}


let pattern = {

    css:{

        capture: '<link href="(.*?.css)" rel="stylesheet">',
        nocapture: '<link href=".*?.css" rel="stylesheet">',

    },

    js: {

        capture: '<script type="text/javascript" src="(.*?.js)"></script>',
        nocapture: '<script type="text/javascript" src=".*?.js"></script>',

    },

}

let index = fs.readFileSync('build/index.html', 'utf8')
let css = pathFromIndex(pattern.css.capture)
let js = pathFromIndex(pattern.js.capture)

let parts = index.split(new RegExp(pattern.css.nocapture + '|' + pattern.js.nocapture))

index = [parts[0], meta, `<style>${css}</style>`, parts[1], `<script type="text/javascript">${js}</script>`, parts[2]].join('')

fs.outputFileSync(outputPath, index, 'utf8')

console.log(`one-file.js:`)
console.log(`write ${`${index.length} chars`.cyan} to ${outputPath}`)
