# H8 engraving w/ gl-react

_**WebGL, Shaders, GLSL**_  
Personalization preview of the Hennessy • 8 carafe  
Demo: https://jniac-tests.gitlab.io/h8-engraving/

## Stack
the famous [`three-js`](https://threejs.org/docs/)  
the well-known [`react-js`](https://reactjs.org/)  
the incredible [`gl-react`](https://github.com/gre/gl-react)  

## EngravingConfig
the app uses global variables:
```javascript
window.EngravingConfig = {

    text: 'hello gl-react & GLSL',   // default text
    showInputs: false,                      // down big buttons and inputs (desktop experience, debug...)
    trapColor: '#f003',                     // color of the trap (tapable area to fake react-native/WebView interaction)

}
window.appInit = props => {...}
```

global function for injectedJavascript, e.g.
```javascript
<WebView
    ref='webView'
    injectedJavaScript={`window.appInit({ text: 'Hello engraving!' })`}
    onMessage={this.onMessage}
/>
```

## Info
rewritten from [a former project](http://hipoly.fr/hennessy/engraving/) (WebGL (threejs) + canvas blending operation)

purpose:
- One single file application (no file dependencies) for further convenient integration in an iOS app (react-native).
- Compatible Safari (iOS)
- WebGL and shaders for better performances

Base64: fonts, image (the carafe)  

After building with react-scripts, HTML, JS and CSS are merged into `build/one-file/index.html` (script: `one-file.js`)

**Note:**  
Some optimizations can be done to divide computation by ± x12!  
Crop, capture once

## Communication
Communication via `window.postMessage(JSON.Stringify({ type: 'text', text }), '*')`  
https://facebook.github.io/react-native/docs/webview.html#onmessage  
https://developer.mozilla.org/fr/docs/Web/API/Window/postMessage

## Install, Build
```shell
npm i
```  
```shell
npm run build # includes 'node one-file.js'
```  
<br>
one-file.js (merge files, called by `npm run build`)  
files merged:  
`build/index.html`  
`build/static/css/main.xxxxxx.css`  
`build/static/js/main.xxxxxx.js`  
```shell
node one-file.js
```  
